---
title: Making the Zombie Factory
header: Welcome, human!
roadmap: roadmap.jpg
path: solidity
---

آیا فکر می‌کنی آنچه را که برای **CryptoZombie** شدن لازم هست، داری؟

این دوره به تو یاد خواهد داد، چطور **بر روی Ethereum یک بازی بسازی**.

این دوره برای تازه‌ واردها به Solidity طراحی شده است، اما فرض می‌کند شما تا حدی با برنامه نویسی در زبانی دیگر تجربه دارید (به عنوان مثال، جاوا اسکریپت).
